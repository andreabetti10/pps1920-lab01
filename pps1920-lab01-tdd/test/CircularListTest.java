import lab01.tdd.CircularList;
import lab01.tdd.CircularListImpl;
import lab01.tdd.SelectStrategy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    CircularList list;

    @BeforeEach
    void beforeEach() {
        list = new CircularListImpl(new ArrayList<>());
    }

    @Test
    void testInitialList() {
        assertTrue(list.isEmpty());
    }

    @Test
    void testAdd() {
        list.add(10);
        assertEquals(1, list.size());
        list.add(10);
        assertEquals(2, list.size());
    }

    @Test
    void testNext() {
        list.add(10);
        list.add(20);
        list.add(30);
        assertEquals(10, list.next().get());
        assertEquals(20, list.next().get());
        assertEquals(30, list.next().get());
        assertEquals(10, list.next().get());
    }

    @Test
    void testPrevious() {
        list.add(10);
        list.add(20);
        list.add(30);
        assertEquals(30, list.previous().get());
        assertEquals(20, list.previous().get());
        assertEquals(10, list.previous().get());
        assertEquals(30, list.previous().get());
    }

    @Test
    void testReset() {
        list.add(10);
        list.add(20);
        list.add(30);
        list.next();
        assertNotEquals(10, list.next().get());
        list.reset();
        assertEquals(10, list.next().get());
    }

    @Test
    void testNextStrategy() {
        SelectStrategy multipleOf10 = ((x) -> (x % 10 == 0));
        list.add(10);
        list.add(15);
        list.add(20);
        list.add(25);
        list.add(30);
        assertEquals(10, list.next(multipleOf10).get());
        assertEquals(20, list.next(multipleOf10).get());
        assertEquals(30, list.next(multipleOf10).get());
    }
}

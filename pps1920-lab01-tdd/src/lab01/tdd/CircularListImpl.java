package lab01.tdd;

import java.util.List;
import java.util.ListIterator;
import java.util.Optional;

public class CircularListImpl implements CircularList {

    List<Integer> list;
    int position = 0;

    public CircularListImpl(List<Integer> list) {
        this.list = list;
    }

    @Override
    public void add(int element) {
        this.list.add(element);
    }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if (list.isEmpty()) {
            return Optional.empty();
        }
        Optional<Integer> next = Optional.ofNullable(list.get(position));
        position = (position == list.size() - 1) ? 0 : position + 1;
        return next;
    }

    @Override
    public Optional<Integer> previous() {
        if (list.isEmpty()) {
            return Optional.empty();
        }
        Optional<Integer> previous = Optional.ofNullable(list.get((position == 0) ? list.size() - 1 : position - 1));
        position = (position == 0) ? list.size() - 1 : position - 1;
        return previous;
    }

    @Override
    public void reset() {
        this.position = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        int i = position;
        while (i < list.size() && !strategy.apply(list.get(i))) {
            this.next();
            i++;
            if (i == list.size()) {
                return Optional.empty();
            }
        }
        return this.next();
    }
}
